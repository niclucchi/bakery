﻿using AutoMapper;
using Bakery.Domain.Models;
using Bakery.DTO.Response;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bakery.Application.Mapping
{
    public sealed class ResponseMappingProfile : Profile
    {
        public ResponseMappingProfile() : base()
        {
            CreateMap(typeof(PaginatedResponse<>), typeof(PaginatedResponse<>));
            CreateMap<Product, ProductDetailResponse>();
            CreateMap<ProductOnSale, ProductOnSaleResponse>()
                .ForMember(dest => dest.FullPrice, opt => opt.MapFrom(src => src.Product.Price))
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.Product.ProductName))
                .ForMember(dest => dest.DiscountedPrice, opt => opt.Ignore())
                .ForMember(dest => dest.IsExpired, opt => opt.Ignore());

            CreateMap<Product, ProductResponse>();
            CreateMap<ProductIngredient,ProductIngredientResponse>();
            CreateMap<Ingredient, IngredientReponse>();
        }
    }
}
