﻿using AutoMapper;
using Bakery.Domain.Models;
using Bakery.DTO.Request;

namespace Bakery.Application.Mapping
{
    public sealed class RequestMappingProfile : Profile
    {
        public RequestMappingProfile() : base()
        {
            CreateMap<CreateProductRequest, Product>()
                .ForMember(dest => dest.ProductIngredients, opt => opt.MapFrom(src => src.ProductIngredients))
                .ForMember(dest => dest.ProductId, opt => opt.Ignore())
                .ForMember(dest => dest.ProductsOnSale, opt => opt.Ignore());

            CreateMap<ProductIngredientRequest, ProductIngredient>()
                .ForMember(dest => dest.ProductId, opt => opt.Ignore())
                .ForMember(dest => dest.Product, opt => opt.Ignore())
                .ForMember(dest => dest.ProductIngredientId, opt => opt.Ignore())
                .ForMember(dest => dest.Ingredient, opt => opt.Ignore());

            CreateMap<CreateProductOnSaleRequest, ProductOnSale>()
                .ForMember(dest => dest.ProductOnSaleId, opt => opt.Ignore())
                .ForMember(dest => dest.PutUpForSaleDateUtc, opt => opt.Ignore())
                .ForMember(dest => dest.Product, opt => opt.Ignore());    

        }
    }
}
