﻿using Bakery.Domain.Repositories;
using Bakery.Domain.Services;
using Bakery.DTO.Request;
using Bakery.DTO.Response;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Application.Services
{
    public class AuthService : IAuthService
    {
        private readonly IUsersRepository usersRepository;
        private readonly IHashingService hashingService;

        public AuthService(IUsersRepository usersRepository, IHashingService hashingService)
        {
            this.usersRepository = usersRepository;
            this.hashingService = hashingService;
        }

        public async Task<bool> CheckCredentialsAsync(AuthenticationRequest request, CancellationToken cancellationToken = default)
        {
            var user = await usersRepository.GetUserAsync(request.Username, cancellationToken);

            if (user != null && hashingService.Check(user.Password, request.Password))
            {
                return true;
            }

            return false;
        }
    }
}
