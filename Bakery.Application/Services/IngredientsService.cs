﻿using AutoMapper;
using Bakery.Domain.Repositories;
using Bakery.Domain.Services;
using Bakery.DTO.Request;
using Bakery.DTO.Response;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Application.Services
{
    public class IngredientsService : IIngredientsService
    {
        private readonly IIngredientsRepository ingredientsRepository;
        private readonly IMapper mapper;

        public IngredientsService(IIngredientsRepository ingredientsRepository, IMapper mapper)
        {
            this.ingredientsRepository = ingredientsRepository;
            this.mapper = mapper;
        }

        public async Task<PaginatedResponse<IngredientReponse>> GetIngredientsAsync(PaginatedRequest request, CancellationToken cancellationToken = default)
        {
            var ingredients = await ingredientsRepository.GetIngredientsAsync(request, cancellationToken);

            return mapper.Map<PaginatedResponse<IngredientReponse>>(ingredients);
        }

    }
}
