﻿using AutoMapper;
using Bakery.Domain.Models;
using Bakery.Domain.Repositories;
using Bakery.Domain.Services;
using Bakery.DTO.Request;
using Bakery.DTO.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Application.Services
{
    public class ProductsService : IProductsService
    {
        private readonly IProductsRepository productsRepository;
        private readonly IMapper mapper;

        public ProductsService(
            IProductsRepository productsRepository,
            IMapper mapper)
        {
            this.productsRepository = productsRepository;
            this.mapper = mapper;
        }

        public async Task<PaginatedResponse<ProductResponse>> GetProductsAsync(PaginatedRequest request, CancellationToken cancellationToken = default)
        {
            var products = await productsRepository.GetProductsAsync(request, cancellationToken);

            return mapper.Map<PaginatedResponse<ProductResponse>>(products);
        }       

        public async Task<ProductDetailResponse> GetProductDetialAsync(int productId, CancellationToken cancellationToken = default)
        {
            var product = await productsRepository.GetProductDetialAsync(productId, cancellationToken);

            return mapper.Map<ProductDetailResponse>(product);
        }

        public async Task<int> CreateProductAsync(CreateProductRequest request, CancellationToken cancellationToken = default)
        {
            var product = mapper.Map<Product>(request);
            await productsRepository.InsertProductAsync(product, cancellationToken);
            await productsRepository.SaveChangesAsync();
            return product.ProductId;
        }

        public async Task UpdateProductAsync(UpdateProductRequest request, CancellationToken cancellationToken = default)
        {
            var product = await productsRepository.GetProductDetialAsync(request.ProductId, cancellationToken);

            product.ProductName = request.ProductName;
            product.Price = request.Price;

            productsRepository.RemoveProductIngredients(product.ProductIngredients);

            product.ProductIngredients = mapper.Map<List<ProductIngredient>>(request.ProductIngredients);

            await productsRepository.SaveChangesAsync(cancellationToken);
        }

        public async Task RemoveProductAsync(int productId, CancellationToken cancellationToken = default)
        {
            productsRepository.RemoveProduct(productId);
            await productsRepository.SaveChangesAsync(cancellationToken);           
        }
    }
}
