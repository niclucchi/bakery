﻿using AutoMapper;
using Bakery.Domain.Models;
using Bakery.Domain.Repositories;
using Bakery.Domain.Services;
using Bakery.DTO.Request;
using Bakery.DTO.Response;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Application.Services
{
    public class ProductsOnSaleService : IProductsOnSaleService
    {
        private readonly IProductsOnSaleRepository productsOnSaleRepository;
        private readonly IProductSaleRuleRepository productSaleRuleRepository;
        private readonly IDiscountCalculatorService discountCalculatorService;
        private readonly IMapper mapper;

        public ProductsOnSaleService(
            IProductsOnSaleRepository productsOnSaleRepository,
            IProductSaleRuleRepository productSaleRuleRepository,
            IDiscountCalculatorService discountCalculatorService,
            IMapper mapper)
        {
            this.productsOnSaleRepository = productsOnSaleRepository;
            this.productSaleRuleRepository = productSaleRuleRepository;
            this.discountCalculatorService = discountCalculatorService;
            this.mapper = mapper;
        }

        public async Task<int> CreateProductOnSaleAsync(CreateProductOnSaleRequest request, CancellationToken cancellationToken = default)
        {
            var productOnSale = await productsOnSaleRepository.GetProductOnSaleAsync(request.ProductId, DateTime.UtcNow.Date, cancellationToken);

            if (productOnSale != null)
            {
                productOnSale.Quantity += request.Quantity;
                await productsOnSaleRepository.SaveChangesAsync();
            }
            else
            {
                productOnSale = mapper.Map<ProductOnSale>(request);
                productOnSale.PutUpForSaleDateUtc = DateTime.UtcNow.Date;
                await productsOnSaleRepository.InsertProductOnSaleAsync(productOnSale, cancellationToken);
                await productsOnSaleRepository.SaveChangesAsync();
            }

            return productOnSale.ProductOnSaleId;
        }

        public async Task<PaginatedResponse<ProductOnSaleResponse>> GetProductsOnSaleAsync(ProductsOnSalePaginatedRequest request, CancellationToken cancellationToken = default)
        {
            var productSaleRules = await productSaleRuleRepository
                .GetProductSaleRulesAsync(cancellationToken);

            var expirationRule = productSaleRules
                .FirstOrDefault(r => !r.CanBeSold);

            var expirationDateUtc = DateTime.UtcNow.Date.AddDays(-expirationRule.DaysFromPutUpForSale);

            var productsOnSale = await productsOnSaleRepository
                .GetProductsOnSaleAsync(request, request.HideExpiredProducts ? expirationDateUtc : (DateTime?)null, cancellationToken);

            var response = mapper.Map<PaginatedResponse<ProductOnSaleResponse>>(productsOnSale);

            foreach (var productOnSale in response.Data)
            {
                productOnSale.DiscountedPrice = discountCalculatorService.CalculateDiscountedPrice(productOnSale, productSaleRules);
                productOnSale.IsExpired = productOnSale.PutUpForSaleDateUtc <= expirationDateUtc;
            }

            return response;
        }

        public async Task RemoveProductOnSaleAsync(int productOnSaleId, CancellationToken cancellation = default)
        {
            productsOnSaleRepository.RemoveProductOnSale(productOnSaleId);
            await productsOnSaleRepository.SaveChangesAsync();
        }

        public async Task UpdateProductOnSaleAsync(UpdateProductOnSaleRequest request, CancellationToken cancellation = default)
        {
            var productOnSale = await productsOnSaleRepository.GetProductOnSaleAsync(request.ProductOnSaleId, cancellation);
            productOnSale.Quantity = request.Quantity;
            await productsOnSaleRepository.SaveChangesAsync();
        }
    }
}
