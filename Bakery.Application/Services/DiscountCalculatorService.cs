﻿using Bakery.Domain.Models;
using Bakery.Domain.Services;
using Bakery.DTO.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bakery.Application.Services
{
    public class DiscountCalculatorService : IDiscountCalculatorService
    {
        public decimal CalculateDiscountedPrice(ProductOnSaleResponse productOnSaleResponse, IEnumerable<ProductSaleRule> productSaleRules)
        {
            var rule = productSaleRules
                .OrderByDescending(r => r.DaysFromPutUpForSale)
                .FirstOrDefault(r => r.DaysFromPutUpForSale <= productOnSaleResponse.DaysFromPutOnSaleDate);

            if(rule != null)
            {
                return productOnSaleResponse.FullPrice * rule.DiscountRate;
            }

            return productOnSaleResponse.FullPrice;
        }
    }
}
