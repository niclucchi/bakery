﻿using Bakery.Domain.Repositories;
using Bakery.DTO.Request;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Application.Validators
{
    public class CreateProductOnSaleRequestValidator : BaseValidator<CreateProductOnSaleRequest>
    {
        private readonly IProductsOnSaleRepository productsOnSaleRepository;

        public CreateProductOnSaleRequestValidator(IProductsOnSaleRepository productsOnSaleRepository)
            :base()
        {
            this.productsOnSaleRepository = productsOnSaleRepository;

            RuleFor(x => x.ProductId)
                .GreaterThan(0);

            RuleFor(x => x.Quantity)
                .GreaterThanOrEqualTo(0);
        }

        public override async Task CustomValidationAsync(ValidationContext<CreateProductOnSaleRequest> context, CreateProductOnSaleRequest request, CancellationToken token)
        {
            
        }
    }
}
