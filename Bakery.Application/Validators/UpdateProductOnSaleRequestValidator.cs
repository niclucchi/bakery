﻿using Bakery.Domain.Repositories;
using Bakery.DTO.Request;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Application.Validators
{
    public class UpdateProductOnSaleRequestValidator : BaseValidator<UpdateProductOnSaleRequest>
    {
        private readonly IProductsOnSaleRepository productsOnSaleRepository;

        public UpdateProductOnSaleRequestValidator(IProductsOnSaleRepository productsOnSaleRepository)
            :base()
        {
            this.productsOnSaleRepository = productsOnSaleRepository;

            RuleFor(x => x.ProductOnSaleId)
                .GreaterThan(0);

            RuleFor(x => x.Quantity)
                .GreaterThanOrEqualTo(0);
        }

        public override async Task CustomValidationAsync(ValidationContext<UpdateProductOnSaleRequest> context, UpdateProductOnSaleRequest request, CancellationToken token)
        {
            var productAlreadyOnSale = await productsOnSaleRepository
                .GetProductOnSaleAsync(request.ProductOnSaleId, DateTime.UtcNow);

            if(productAlreadyOnSale == null)
            {
                context.AddFailure($"Product on sale {request.ProductOnSaleId} does not exists");
            }
        }
    }
}
