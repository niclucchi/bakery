﻿using Bakery.DTO.Request;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Application.Validators
{
    public class CreateProductRequestValidator : BaseValidator<CreateProductRequest>
    {
        public CreateProductRequestValidator()
            : base()
        {
            RuleFor(x => x.ProductName)
                .NotNull()
                .NotEmpty();

            RuleFor(x => x.Price)
                .GreaterThan(0);

        }

        public override Task CustomValidationAsync(ValidationContext<CreateProductRequest> context, CreateProductRequest request, CancellationToken token)
        {
            return Task.CompletedTask;
        }
    }
}
