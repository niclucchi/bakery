﻿using FluentValidation;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Application.Validators
{
    public abstract class BaseValidator<T> : AbstractValidator<T>
    {
        public BaseValidator() : base()
        {
            this.CascadeMode = CascadeMode.Stop;

            RuleFor(request => request).CustomAsync(async (request, context, token) =>
            {
                await this.CustomValidationAsync(context, request, token);
            });
        }

        public abstract Task CustomValidationAsync(ValidationContext<T> context, T request, CancellationToken token);
    }
}
