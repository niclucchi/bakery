﻿using Bakery.Domain.Repositories;
using Bakery.DTO.Request;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Application.Validators
{
    public class UpdateProductRequestValidator : BaseValidator<UpdateProductRequest>
    {
        private readonly IProductsRepository productsRepository;

        public UpdateProductRequestValidator(IProductsRepository productsRepository)
            : base()
        {
            RuleFor(x => x.ProductName)
                .NotNull()
                .NotEmpty();

            RuleFor(x => x.Price)
                .GreaterThan(0);

            RuleFor(x => x.ProductId)
                .GreaterThan(0);
            this.productsRepository = productsRepository;
        }

        public override async Task CustomValidationAsync(ValidationContext<UpdateProductRequest> context, UpdateProductRequest request, CancellationToken token)
        {
            var product = await productsRepository.GetProductAsync(request.ProductId, token);

            if (product == null)
            {
                context.AddFailure($"Product {request.ProductId} does not exists");
            }
        }
    }
}
