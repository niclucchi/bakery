﻿using AutoMapper;
using Bakery.Application.Mapping;

namespace Bakery.Test.Unit.Helpers
{
    public static class MappingHelper
    {
        public static IMapper ConfigureAutomapper()
        {
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new ResponseMappingProfile());
                mc.AddProfile(new RequestMappingProfile());
            });

            IMapper mapper = mapperConfig.CreateMapper();

            return mapper;
        }
    }
}
