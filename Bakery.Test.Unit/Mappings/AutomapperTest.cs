﻿using Bakery.Test.Unit.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bakery.Test.Unit.Mappings
{
    [TestClass]
    public class AutomapperTest 
    {
        [TestMethod]
        public void AutomapperConfiguration_ShouldBeValid()
        {
            var mapper = MappingHelper.ConfigureAutomapper();            
            mapper.ConfigurationProvider.AssertConfigurationIsValid();
        }
    }
}
