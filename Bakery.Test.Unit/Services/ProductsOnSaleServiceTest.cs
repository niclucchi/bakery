﻿using AutoMapper;
using Bakery.Application.Services;
using Bakery.Domain.Models;
using Bakery.Domain.Repositories;
using Bakery.Domain.Services;
using Bakery.DTO.Request;
using Bakery.DTO.Response;
using Bakery.Test.Unit.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Test.Unit.Services
{
    [TestClass]
    public class ProductsOnSaleServiceTest
    {
        public ProductsOnSaleService productsOnSaleService;
        public Mock<IProductsOnSaleRepository> productsOnSaleRepositoryMock;
        public Mock<IProductSaleRuleRepository> productSaleRuleRepositoryMock;
        public Mock<IDiscountCalculatorService> discountCalculatorService;

        [TestInitialize]
        public void TestInit()
        {
            productsOnSaleRepositoryMock = new Mock<IProductsOnSaleRepository>();
            productSaleRuleRepositoryMock = new Mock<IProductSaleRuleRepository>();
            discountCalculatorService = new Mock<IDiscountCalculatorService>();


            productsOnSaleService = new ProductsOnSaleService(productsOnSaleRepositoryMock.Object, productSaleRuleRepositoryMock.Object, discountCalculatorService.Object, MappingHelper.ConfigureAutomapper());

        }

        [TestMethod]
        public async Task GetProductsOnSaleAsync_ShouldCalculateDiscountedPrices()
        {
            GetProductsOnSaleAsync_SetupSuccessCase();

            var request = new ProductsOnSalePaginatedRequest();
            request.HideExpiredProducts = true;
            var response = await productsOnSaleService.GetProductsOnSaleAsync(request);

            productSaleRuleRepositoryMock.Verify(mock => mock.GetProductSaleRulesAsync(It.IsAny<CancellationToken>()), Times.Once());

            productsOnSaleRepositoryMock.Verify(mock => mock.GetProductsOnSaleAsync(
                It.Is<PaginatedRequest>(x => x == request), 
                It.Is<DateTime?>(x => x == DateTime.UtcNow.Date.AddDays(-2)),
                It.IsAny<CancellationToken>()), Times.Once());

            discountCalculatorService.Verify(mock => mock.CalculateDiscountedPrice(It.IsAny<ProductOnSaleResponse>(), It.IsAny<IEnumerable<ProductSaleRule>>()), Times.Exactly(2));

            Assert.IsNotNull(response);
            Assert.IsNotNull(response.Data);
            Assert.AreEqual(2, response.Data.Count);
            Assert.AreEqual(8, response.Data[0].DiscountedPrice);
            Assert.AreEqual(16, response.Data[1].DiscountedPrice);
        }


        private void GetProductsOnSaleAsync_SetupSuccessCase()
        {
            productSaleRuleRepositoryMock
               .Setup(x => x.GetProductSaleRulesAsync(It.IsAny<CancellationToken>()))
               .ReturnsAsync(new List<ProductSaleRule>()
               {
                   new ProductSaleRule()
                   {
                       DaysFromPutUpForSale = 1, 
                       DiscountRate = 0.8M,
                       CanBeSold = true
                   },
                   new ProductSaleRule()
                   {
                       DaysFromPutUpForSale = 2,
                       DiscountRate = 0.8M,
                       CanBeSold = false
                   }
               });

            productsOnSaleRepositoryMock
               .Setup(x => x.GetProductsOnSaleAsync(It.IsAny<PaginatedRequest>(), It.IsAny<DateTime?>(), It.IsAny<CancellationToken>()))
               .ReturnsAsync(new PaginatedResponse<ProductOnSale>() 
               {
                    Data = new List<ProductOnSale>()
                    {
                        new ProductOnSale()
                        {
                            Quantity = 2,
                            PutUpForSaleDateUtc = DateTime.UtcNow,
                            Product = new Product()
                            {

                                ProductId = 1,
                                Price = 10
                            }
                        },
                         new ProductOnSale()
                        {
                            Quantity = 3,
                            PutUpForSaleDateUtc = DateTime.UtcNow,
                            Product = new Product()
                            {
                                ProductId = 2,
                                Price = 20
                            }
                        },
                    }
               });

            discountCalculatorService
              .Setup(x => x.CalculateDiscountedPrice(It.IsAny<ProductOnSaleResponse>(), It.IsAny<IEnumerable<ProductSaleRule>>()))
              .Returns((ProductOnSaleResponse a, IEnumerable<ProductSaleRule> b) => { return a.FullPrice * 0.8M ; });
        }

    }
}
