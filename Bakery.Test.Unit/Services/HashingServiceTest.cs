﻿using Bakery.Application.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bakery.Test.Unit.Services
{
    [TestClass]
    public class HashingServiceTest
    {
        [DataRow("test!&s32ABC423")]
        [DataRow("test")]
        [DataTestMethod]
        public void Check_ShouldReturnTrue_ForSameString(string test)
        {
            var hashingService = new HashingService();
            string hash = hashingService.Hash(test);
            bool check = hashingService.Check(hash, test);

            Assert.IsTrue(check);
        }

        [DataRow("test!&s32ABC423")]
        [DataRow("test")]
        [DataTestMethod]
        public void Check_ShouldReturnFalse_ForDifferentString(string test)
        {
            var hashingService = new HashingService();
            string hash = hashingService.Hash(test);

            bool check = hashingService.Check(hash, test + "a");

            Assert.IsFalse(check);
        }

    }    
}
