﻿using Bakery.Application.Services;
using Bakery.Domain.Models;
using Bakery.DTO.Response;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bakery.Test.Unit.Services
{
    [TestClass]
    public class DiscountCalculatorServiceTest
    {

        [DataTestMethod]
        [DataRow(0, 10, 10)]
        [DataRow(1, 10, 8)]
        [DataRow(2, 10, 2)]
        [DataRow(3, 10, 0)]
        [DataRow(4, 10, 0)]
        public void CalculateDiscountedPrice_OK(int daysFromPutUpForSale, int fullPrice, int expectedPrice)
        {
            var service = new DiscountCalculatorService();

            var productOnSale = new ProductOnSaleResponse()
            {
                FullPrice = (decimal)fullPrice,
                PutUpForSaleDateUtc = DateTime.UtcNow.Date.AddDays(-daysFromPutUpForSale)
            };

            var productSaleRules = new List<ProductSaleRule>()
            {
                new ProductSaleRule()
                {
                    DiscountRate = 1,
                    DaysFromPutUpForSale = 0,
                    ProductSaleRuleId = 1
                },
                new ProductSaleRule()
                {
                    DiscountRate = 0.8M,
                    DaysFromPutUpForSale = 1,
                    ProductSaleRuleId = 2
                },
                new ProductSaleRule()
                {
                    DiscountRate = 0.2M,
                    DaysFromPutUpForSale = 2,
                    ProductSaleRuleId = 2
                },
                new ProductSaleRule()
                {
                    DiscountRate = 0M,
                    DaysFromPutUpForSale = 3,
                    ProductSaleRuleId = 4
                },
            };

            var result = service.CalculateDiscountedPrice(productOnSale, productSaleRules);

            Assert.AreEqual((decimal)expectedPrice, result);
        }
    }
}
