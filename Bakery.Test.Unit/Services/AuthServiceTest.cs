﻿using Bakery.Application.Services;
using Bakery.Domain.Models;
using Bakery.Domain.Repositories;
using Bakery.Domain.Services;
using Bakery.DTO.Request;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Test.Unit.Services
{
    [TestClass]
    public class AuthServiceTest
    {
        public AuthService authService;
        public Mock<IUsersRepository> userRepositoryMock;
        public Mock<IHashingService> hashingServiceMock;

        [TestInitialize]
        public void TestInit()
        {
            userRepositoryMock = new Mock<IUsersRepository>();
            hashingServiceMock = new Mock<IHashingService>();

            authService = new AuthService(userRepositoryMock.Object, hashingServiceMock.Object);
        }

        [TestMethod]
        public async Task CheckCredentialsAsync_ShouldReturnFalse_IfUserDoesNotExists()
        {
            SetupSuccessCase();

            userRepositoryMock
               .Setup(x => x.GetUserAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
               .ReturnsAsync(value: null);

            bool check = await authService.CheckCredentialsAsync(new AuthenticationRequest());

            Assert.IsFalse(check);
        }

        [TestMethod]
        public async Task CheckCredentialsAsync_ShouldReturnFalse_IfHashDoesNotMatch()
        {
            SetupSuccessCase();

            hashingServiceMock
                .Setup(x => x.Check(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(false);

            bool check = await authService.CheckCredentialsAsync(new AuthenticationRequest());

            Assert.IsFalse(check);
        }

        [TestMethod]
        public async Task CheckCredentialsAsync_ShouldReturnTrue_IfCredentialsAreCorrect()
        {
            SetupSuccessCase();

            var request = new AuthenticationRequest()
            {
                Username = "a@a.a",
                Password = "password"
            };

            bool check = await authService.CheckCredentialsAsync(request);

            userRepositoryMock.Verify(mock => mock.GetUserAsync(It.Is<string>(x => x == "a@a.a"), It.IsAny<CancellationToken>()), Times.Once());
            hashingServiceMock.Verify(mock => mock.Check(It.Is<string>(x => x == "hashedpassword"), It.Is<string>(x => x == "password")), Times.Once());

            Assert.IsTrue(check);
        }

        private void SetupSuccessCase()
        {
            userRepositoryMock
                .Setup(x => x.GetUserAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new User() { Email = "a@a.a", Password = "hashedpassword" });

            hashingServiceMock
                .Setup(x => x.Check(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(true);

        }
    }
}
