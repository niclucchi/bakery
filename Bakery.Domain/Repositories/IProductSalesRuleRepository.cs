﻿using Bakery.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Domain.Repositories
{
    public interface IProductSaleRuleRepository
    {
        public Task<List<ProductSaleRule>> GetProductSaleRulesAsync(CancellationToken cancellationToken = default);
    }
}
