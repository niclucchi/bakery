﻿using Bakery.Domain.Models;
using Bakery.DTO.Request;
using Bakery.DTO.Response;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Domain.Repositories
{
    public interface IProductsRepository
    {
        Task<List<ProductIngredient>> GetProductIngredientsAsync(int productId, CancellationToken cancellationToken = default);
        Task<Product> GetProductAsync(int productId, CancellationToken cancellationToken = default);
        Task<Product> GetProductDetialAsync(int productId, CancellationToken cancellationToken = default);
        Task<PaginatedResponse<Product>> GetProductsAsync(PaginatedRequest request, CancellationToken cancellationToken = default);
        Task InsertProductAsync(Product product, CancellationToken cancellationToken = default);
        void RemoveProduct(int productId);
        void RemoveProductIngredients(IEnumerable<ProductIngredient> productIngredients);
        Task SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}
