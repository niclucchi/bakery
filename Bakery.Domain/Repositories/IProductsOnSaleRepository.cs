﻿using Bakery.Domain.Models;
using Bakery.DTO.Request;
using Bakery.DTO.Response;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Domain.Repositories
{
    public interface IProductsOnSaleRepository
    {
        Task<ProductOnSale> GetProductOnSaleAsync(int productId, DateTime date, CancellationToken cancellationToken = default);
        Task<ProductOnSale> GetProductOnSaleAsync(int productOnSaleId, CancellationToken cancellationToken = default);
        Task<PaginatedResponse<ProductOnSale>> GetProductsOnSaleAsync(PaginatedRequest request, DateTime? maxDate = null, CancellationToken cancellationToken = default);
        Task InsertProductOnSaleAsync(ProductOnSale productOnSale, CancellationToken cancellationToken = default);
        void RemoveProductOnSale(int productOnSaleId);
        Task SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}
