﻿using Bakery.Domain.Models;
using Bakery.DTO.Request;
using Bakery.DTO.Response;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Domain.Repositories
{
    public interface IIngredientsRepository
    {
        Task InsertIngredientsAsync(IEnumerable<Ingredient> ingredients, CancellationToken cancellationToken);
        void DeleteIngredients(IEnumerable<Ingredient> ingredients);
        Task<PaginatedResponse<Ingredient>> GetIngredientsAsync(PaginatedRequest request, CancellationToken cancellationToken);
    }
}
