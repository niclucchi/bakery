﻿using Bakery.Domain.Models;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Domain.Repositories
{
    public interface IUsersRepository
    {
        Task<User> GetUserAsync(string username, CancellationToken cancellationToken = default);
    }
}
