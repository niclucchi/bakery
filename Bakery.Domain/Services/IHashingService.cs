﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bakery.Domain.Services
{
    public interface IHashingService
    {
        string Hash(string password);
        bool Check(string hash, string password);
    }
}
