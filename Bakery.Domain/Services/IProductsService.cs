﻿using Bakery.DTO.Request;
using Bakery.DTO.Response;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Domain.Services
{
    public interface IProductsService
    {
        Task<int> CreateProductAsync(CreateProductRequest request, CancellationToken cancellationToken = default);
        Task<ProductDetailResponse> GetProductDetialAsync(int productId, CancellationToken cancellationToken = default);
        Task<PaginatedResponse<ProductResponse>> GetProductsAsync(PaginatedRequest request, CancellationToken cancellationToken = default);
        Task RemoveProductAsync(int productId, CancellationToken cancellationToken = default);
        Task UpdateProductAsync(UpdateProductRequest request, CancellationToken cancellationToken = default);
    }
}
