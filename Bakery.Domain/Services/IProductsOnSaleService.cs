﻿using Bakery.DTO.Request;
using Bakery.DTO.Response;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Domain.Services
{
    public interface IProductsOnSaleService
    {
        Task<PaginatedResponse<ProductOnSaleResponse>> GetProductsOnSaleAsync(ProductsOnSalePaginatedRequest request, CancellationToken cancellationToken = default);
        Task<int> CreateProductOnSaleAsync(CreateProductOnSaleRequest request, CancellationToken cancellation = default);
        Task UpdateProductOnSaleAsync(UpdateProductOnSaleRequest request, CancellationToken cancellation = default);
        Task RemoveProductOnSaleAsync(int productOnSaleId, CancellationToken cancellation = default);
    }
}
