﻿using Bakery.DTO.Request;
using Bakery.DTO.Response;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Domain.Services
{
    public interface IIngredientsService
    {
        Task<PaginatedResponse<IngredientReponse>> GetIngredientsAsync(PaginatedRequest request, CancellationToken cancellationToken = default);
    }
}
