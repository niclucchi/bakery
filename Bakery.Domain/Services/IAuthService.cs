﻿using Bakery.DTO.Request;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Domain.Services
{
    public interface IAuthService
    {
        Task<bool> CheckCredentialsAsync(AuthenticationRequest request, CancellationToken cancellationToken = default);
    }
}
