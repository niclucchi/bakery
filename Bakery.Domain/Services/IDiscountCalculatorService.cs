﻿using Bakery.Domain.Models;
using Bakery.DTO.Response;
using System;
using System.Collections.Generic;
using System.Text;

namespace Bakery.Domain.Services
{
    public interface IDiscountCalculatorService
    {
        decimal CalculateDiscountedPrice(ProductOnSaleResponse productOnSaleResponse, IEnumerable<ProductSaleRule> productSaleRules);
    }
}
