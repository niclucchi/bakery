﻿using System;
namespace Bakery.Domain.Models
{
    public class ProductOnSale
    {
        public int ProductOnSaleId { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public int Quantity { get; set; }
        public DateTime PutUpForSaleDateUtc { get; set; }        
        public int DaysFromPutOnSaleDate => Convert.ToInt32((DateTime.UtcNow.Date - PutUpForSaleDateUtc).TotalDays);
    }
}
