﻿using System;
using System.Collections.Generic;

namespace Bakery.Domain.Models
{
    public class Product
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public ICollection<ProductOnSale> ProductsOnSale { get; set; }
        public ICollection<ProductIngredient> ProductIngredients { get; set; }

        public Product()
        {
            ProductsOnSale = new HashSet<ProductOnSale>();
            ProductIngredients = new HashSet<ProductIngredient>();
        }
    }
}
