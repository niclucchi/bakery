﻿using System;
namespace Bakery.Domain.Models
{
    public class ProductSaleRule
    {
        public int ProductSaleRuleId { get; set; }
        public int DaysFromPutUpForSale { get; set; }
        public decimal DiscountRate { get; set; }
        public bool CanBeSold { get; set; }
    }
}
