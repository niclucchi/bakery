﻿using Bakery.Common.Enums;

namespace Bakery.Domain.Models
{
    public class ProductIngredient
    {
        public int ProductIngredientId { get; set; }
        public int IngredientId { get; set; }
        public Ingredient Ingredient { get; set; }
        public double Quantity { get; set; }
        public UnitOfMeasureEnum UnitOfMeasure { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
    }
}
