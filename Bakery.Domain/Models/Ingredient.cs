﻿using System;
using System.Collections.Generic;

namespace Bakery.Domain.Models
{
    public class Ingredient
    {
        public int IngredientId { get; set; }
        public string Name { get; set; }
        public ICollection<ProductIngredient> ProductIngredients { get; set; }

        public Ingredient()
        {
            ProductIngredients = new HashSet<ProductIngredient>();
        }
    }
}
