﻿using Bakery.Domain.Models;
using Bakery.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Data.SqlServer.Repositories
{
    public class UsersRepository : IUsersRepository
    {
        private readonly BakeryDbContext bakeryDbContext;

        public UsersRepository(BakeryDbContext bakeryDbContext)
        {
            this.bakeryDbContext = bakeryDbContext;
        }

        public Task<User> GetUserAsync(string username, CancellationToken cancellationToken = default)
        {
            return bakeryDbContext
                .Users
                .FirstOrDefaultAsync(u => u.Email == username);
        }
    }
}
