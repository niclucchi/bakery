﻿using Bakery.Data.SqlServer.Extensions;
using Bakery.Domain.Models;
using Bakery.Domain.Repositories;
using Bakery.DTO.Request;
using Bakery.DTO.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Data.SqlServer.Repositories
{
    public class IngredientsRepository : IIngredientsRepository
    {
        private readonly BakeryDbContext bakeryDbContext;

        public IngredientsRepository(BakeryDbContext bakeryDbContext)
        {
            this.bakeryDbContext = bakeryDbContext;
        }
        public void DeleteIngredients(IEnumerable<Ingredient> ingredients)
        {
            bakeryDbContext.Ingredients.RemoveRange(ingredients);
        }

        public Task<PaginatedResponse<Ingredient>> GetIngredientsAsync(PaginatedRequest request, CancellationToken cancellationToken)
        {
            var queryable = bakeryDbContext
                .Ingredients
                .AsQueryable();

            if (!string.IsNullOrWhiteSpace(request.Search))
            {
                queryable = queryable.Where(i => i.Name.Contains(request.Search));
            }

            return queryable
                .ToPaginatedResponseAsync(request, cancellationToken);
        }

        public Task InsertIngredientsAsync(IEnumerable<Ingredient> ingredients, CancellationToken cancellationToken)
        {
            return bakeryDbContext
                .Ingredients
                .AddRangeAsync(ingredients, cancellationToken);
        }
    }
}
