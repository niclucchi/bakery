﻿using Bakery.Domain.Models;
using Bakery.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Data.SqlServer.Repositories
{
    public class ProductSaleRuleRepository : IProductSaleRuleRepository
    {
        private readonly BakeryDbContext bakeryDbContext;
        private readonly IMemoryCache memoryCache;
        public const string PRODUCT_SALE_RULE_CACHE_KEY = "PRODUCT_SALE_RULE_CACHE_KEY";

        public ProductSaleRuleRepository(BakeryDbContext bakeryDbContext, IMemoryCache memoryCache)
        {
            this.bakeryDbContext = bakeryDbContext;
            this.memoryCache = memoryCache;
        }
        public async Task<List<ProductSaleRule>> GetProductSaleRulesAsync(CancellationToken cancellationToken = default)
        {
            if (!memoryCache.TryGetValue(PRODUCT_SALE_RULE_CACHE_KEY, out List<ProductSaleRule> productSaleRules))
            {
                productSaleRules = await bakeryDbContext
                    .ProductSaleRules
                    .ToListAsync();

                memoryCache.Set(PRODUCT_SALE_RULE_CACHE_KEY, productSaleRules);
            }

            return productSaleRules;
        }
    }
}
