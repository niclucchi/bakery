﻿using Bakery.Data.SqlServer.Extensions;
using Bakery.Domain.Models;
using Bakery.Domain.Repositories;
using Bakery.DTO.Request;
using Bakery.DTO.Response;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Data.SqlServer.Repositories
{
    public class ProductsRepository : IProductsRepository
    {
        private readonly BakeryDbContext bakeryDbContext;

        public ProductsRepository(BakeryDbContext bakeryDbContext)
        {
            this.bakeryDbContext = bakeryDbContext;
        }

        public Task<Product> GetProductAsync(int productId, CancellationToken cancellationToken = default)
        {
            return bakeryDbContext
                .Products
                .FirstOrDefaultAsync(p => p.ProductId == productId, cancellationToken);
        }

        public Task<Product> GetProductDetialAsync(int productId, CancellationToken cancellationToken = default)
        {
            return bakeryDbContext
                .Products
                .Include(p => p.ProductIngredients).ThenInclude(i => i.Ingredient)
                .FirstOrDefaultAsync(p => p.ProductId == productId, cancellationToken);
        }

        public Task<List<ProductIngredient>> GetProductIngredientsAsync(int productId, CancellationToken cancellationToken = default)
        {
            return bakeryDbContext
                .ProductIngredients
                .Where(p => p.ProductId == productId)
                .ToListAsync(cancellationToken);
        }

        public Task<PaginatedResponse<Product>> GetProductsAsync(PaginatedRequest request, CancellationToken cancellationToken = default)
        {
            var queryable = bakeryDbContext
                .Products
                .AsQueryable();

            if (!string.IsNullOrWhiteSpace(request.Search))
            {
                queryable = queryable.Where(i => i.ProductName.Contains(request.Search));
            }


            return queryable
                .ToPaginatedResponseAsync(request, cancellationToken);
        }

        public async Task InsertProductAsync(Product product, CancellationToken cancellationToken = default)
        {
            await bakeryDbContext
                .Products
                .AddAsync(product, cancellationToken);
        }

        public void RemoveProduct(int productId)
        {
            var product = new Product()
            {
                ProductId = productId
            };

            bakeryDbContext.Entry(product).State = EntityState.Deleted;
        }

        public void RemoveProductIngredients(IEnumerable<ProductIngredient> productIngredients)
        {
            bakeryDbContext
                .ProductIngredients
                .RemoveRange(productIngredients);
        }

        public Task SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return bakeryDbContext.SaveChangesAsync();
        }
    }
}
