﻿using Bakery.Data.SqlServer.Extensions;
using Bakery.Domain.Models;
using Bakery.Domain.Repositories;
using Bakery.DTO.Request;
using Bakery.DTO.Response;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Data.SqlServer.Repositories
{
    public class ProductsOnSaleRepository : IProductsOnSaleRepository
    {
        private readonly BakeryDbContext bakeryDbContext;

        public ProductsOnSaleRepository(BakeryDbContext bakeryDbContext)
        {
            this.bakeryDbContext = bakeryDbContext;
        }

        public Task<ProductOnSale> GetProductOnSaleAsync(int productOnSaleId, CancellationToken cancellationToken = default)
        {
            return bakeryDbContext
                .ProductsOnSale
                .FirstOrDefaultAsync(p => p.ProductOnSaleId == productOnSaleId);
        }

        public Task<ProductOnSale> GetProductOnSaleAsync(int productId, DateTime dateUtc, CancellationToken cancellationToken = default)
        {
            return bakeryDbContext
                .ProductsOnSale
                .FirstOrDefaultAsync(p => p.ProductId == productId && p.PutUpForSaleDateUtc == dateUtc);
        }

        public Task<PaginatedResponse<ProductOnSale>> GetProductsOnSaleAsync(PaginatedRequest request, DateTime? maxDate = null, CancellationToken cancellationToken = default)
        {
            var queryable = bakeryDbContext
                .ProductsOnSale
                .Include(p => p.Product)
                .AsQueryable();

            if(maxDate.HasValue)
            {
                queryable = queryable.Where(p => p.PutUpForSaleDateUtc > maxDate);
            }

            if (!string.IsNullOrWhiteSpace(request.Search))
            {
                queryable = queryable.Where(i => i.Product.ProductName.Contains(request.Search));
            }

            return queryable
                .ToPaginatedResponseAsync(request);
        }

        public async Task InsertProductOnSaleAsync(ProductOnSale productOnSale, CancellationToken cancellationToken = default)
        {
            await bakeryDbContext.ProductsOnSale.AddAsync(productOnSale);
        }

        public void RemoveProductOnSale(int productOnSaleId)
        {
            var productOnSale = new ProductOnSale()
            {
                ProductOnSaleId = productOnSaleId
            };

            bakeryDbContext.Entry(productOnSale).State = EntityState.Deleted;
        }

        public async Task SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            await bakeryDbContext.SaveChangesAsync(cancellationToken);
        }
    }
}
