﻿using Bakery.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Bakery.Data.SqlServer
{
    public class BakeryDbContext : DbContext
    {
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<ProductSaleRule> ProductSaleRules { get; set; }
        public virtual DbSet<Ingredient> Ingredients { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<ProductOnSale> ProductsOnSale { get; set; }
        public virtual DbSet<ProductIngredient> ProductIngredients { get; set; }


        public BakeryDbContext(DbContextOptions<BakeryDbContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            Assembly assemblyWithConfigurations = GetType().Assembly;
            modelBuilder.ApplyConfigurationsFromAssembly(assemblyWithConfigurations);
        }
    }
}
