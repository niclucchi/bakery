﻿using Bakery.DTO.Request;
using Bakery.DTO.Response;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Linq.Dynamic.Core;
using System.Threading;

namespace Bakery.Data.SqlServer.Extensions
{
    public static class IQueryableExtensions
    {
        public static async Task<PaginatedResponse<TResult>> ToPaginatedResponseAsync<TResult>(this IQueryable<TResult> queryable, PaginatedRequest query, CancellationToken cancellationToken = default ) where TResult : class
        {
            string sorting = query.GetSorting();

            if (!string.IsNullOrWhiteSpace(sorting))
                queryable = queryable.OrderBy(sorting);

            long totalRecords = await queryable.LongCountAsync(cancellationToken);

            int pageCount = GetCount(totalRecords, query.PageSize);

            int skip = GetSkip(totalRecords, query.PageNum, query.PageSize);

            List<TResult> data = await queryable
                .AsNoTracking()
                .Skip(skip)
                .Take(query.PageSize)
                .ToListAsync(cancellationToken);

            return new PaginatedResponse<TResult>()
            {
                Data = data,
                NumberOfPages = pageCount,
                CurrentPage = query.PageNum,
                PageSize = query.PageSize,
                TotalRecords = totalRecords
            };
        }

        public static async Task<PaginatedResponse<TResult>> ToPaginatedResultAsync<TSource, TResult>(this IQueryable<TSource> queryable, PaginatedRequest query, Expression<Func<TSource, TResult>> selector) where TSource : class
        {
            string sorting = query.GetSorting();

            if (!string.IsNullOrWhiteSpace(sorting))
                queryable = queryable.OrderBy(sorting);

            long totalRecords = await queryable.LongCountAsync();

            int pageCount = GetCount(totalRecords, query.PageSize);

            int skip = GetSkip(totalRecords, query.PageNum, query.PageSize);

            List<TResult> data = await queryable
                .AsNoTracking()
                .Select(selector)
                .Skip(skip)
                .Take(query.PageSize)
                .ToListAsync();

            return new PaginatedResponse<TResult>()
            {
                Data = data,
                NumberOfPages = pageCount,
                CurrentPage = query.PageNum,
                PageSize = query.PageSize,
                TotalRecords = totalRecords
            };
        }

        public static int GetSkip(long totalRecords, int pageNum, int pageSize)
        {
            return (totalRecords <= pageSize)
                ? 0
                : (pageNum - 1) * pageSize;

        }

        public static int GetCount(long totalRecords, int pageSize)
        {
            return (int)Math.Ceiling((double)totalRecords / pageSize);
        }
    }
}
