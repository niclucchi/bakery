﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Bakery.Data.SqlServer.Migrations
{
    public partial class seed_database : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO Ingredients (Name) VALUES ('Eggs')");
            migrationBuilder.Sql("INSERT INTO Ingredients (Name) VALUES ('Flour')");
            migrationBuilder.Sql("INSERT INTO Ingredients (Name) VALUES ('Water')");
            migrationBuilder.Sql("INSERT INTO Ingredients (Name) VALUES ('Sugar')");
            migrationBuilder.Sql("INSERT INTO Ingredients (Name) VALUES ('Milk')");
            migrationBuilder.Sql("INSERT INTO Ingredients (Name) VALUES ('Butter')");

            migrationBuilder.Sql("INSERT INTO ProductSaleRules (DaysFromPutUpForSale,DiscountRate,CanBeSold) VALUES (0, 1, 1)");
            migrationBuilder.Sql("INSERT INTO ProductSaleRules (DaysFromPutUpForSale,DiscountRate,CanBeSold) VALUES (1, 0.8, 1)");
            migrationBuilder.Sql("INSERT INTO ProductSaleRules (DaysFromPutUpForSale,DiscountRate,CanBeSold) VALUES (2, 0.2, 1)");
            migrationBuilder.Sql("INSERT INTO ProductSaleRules (DaysFromPutUpForSale,DiscountRate,CanBeSold) VALUES (3, 0, 0)");

            migrationBuilder.Sql("INSERT INTO Users (Email, Password) VALUES ('luana@gmail.com', '10000.9p3TjCP3Wx8rh29MVmX2FQ==.dBCz+SGsBkA8isdnzJw7nPKRSwY7NSMZxjyQ9WdUH5Y=')");
            migrationBuilder.Sql("INSERT INTO Users (Email, Password) VALUES ('maria@gmail.com', '10000.9p3TjCP3Wx8rh29MVmX2FQ==.dBCz+SGsBkA8isdnzJw7nPKRSwY7NSMZxjyQ9WdUH5Y=')");

            migrationBuilder.Sql("INSERT INTO Products (ProductName, Price) VALUES ('Pizza', 1.5)");
            migrationBuilder.Sql("INSERT INTO Products (ProductName, Price) VALUES ('Bread', 1)");
            migrationBuilder.Sql("INSERT INTO Products (ProductName, Price) VALUES ('Cake', 2)");

            migrationBuilder.Sql("INSERT INTO ProductsOnSale (ProductId, Quantity, PutUpForSaleDateUtc) VALUES ((SELECT ProductId FROM PRODUCTS WHERE ProductName = 'Pizza'), 10, CONVERT(DATE,DATEADD(DAY, -1, GETUTCDATE())))");
            migrationBuilder.Sql("INSERT INTO ProductsOnSale (ProductId, Quantity, PutUpForSaleDateUtc) VALUES ((SELECT ProductId FROM PRODUCTS WHERE ProductName = 'Bread'), 12, CONVERT(DATE,DATEADD(DAY, -2, GETUTCDATE())))");
            migrationBuilder.Sql("INSERT INTO ProductsOnSale (ProductId, Quantity, PutUpForSaleDateUtc) VALUES ((SELECT ProductId FROM PRODUCTS WHERE ProductName = 'Cake'), 8, CONVERT(DATE,DATEADD(DAY, -4, GETUTCDATE())))");

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM Ingredients");
            migrationBuilder.Sql("DELETE FROM ProductSaleRules");
            migrationBuilder.Sql("DELETE FROM Users");
            migrationBuilder.Sql("DELETE FROM ProductsOnSale");
            migrationBuilder.Sql("DELETE FROM Products");
        }
    }
}
