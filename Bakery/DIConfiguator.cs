﻿using Bakery.Application.Services;
using Bakery.Common.Settings;
using Bakery.Data.SqlServer;
using Bakery.Data.SqlServer.Repositories;
using Bakery.Domain.Repositories;
using Bakery.Domain.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bakery
{
    public static class DIConfiguator
    {
        public static IServiceCollection AddCorsForSpaDevServer(this IServiceCollection serviceDescriptors)
        {
            var provider = serviceDescriptors.BuildServiceProvider();
            var environment = provider.GetService<Microsoft.AspNetCore.Hosting.IWebHostEnvironment>();

            if (environment.IsDevelopment())
            {
                serviceDescriptors.AddCors(option =>
                {
                    option.AddPolicy("default", policy =>
                    {
                        policy.AllowAnyOrigin()
                            .AllowAnyHeader()
                            .AllowAnyMethod()
                            .WithExposedHeaders("Content-Disposition");
                    });
                });
            }
            return serviceDescriptors;
        }
        public static IServiceCollection AddJwtAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            services
               .AddAuthentication(x =>
               {
                   x.DefaultAuthenticateScheme = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme;
                   x.DefaultChallengeScheme = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme;
               })
               .AddJwtBearer(options =>
               {
                   options.RequireHttpsMetadata = true;
                   options.SaveToken = true;
                   options.TokenValidationParameters = new TokenValidationParameters
                   {
                       ValidateIssuer = true,
                       ValidateAudience = true,
                       ValidateLifetime = true,
                       ValidateIssuerSigningKey = true,
                       ValidIssuer = configuration["BakerySettings.JwtIssuer"],
                       ValidAudience = configuration["BakerySettings.JwtIssuer"],
                       IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["BakerySettings.JwtKey"]))
                   };
               });

            return services;
        }

        public static IServiceCollection RegisterDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton(sp =>
            {
                return new DbContextOptionsBuilder<BakeryDbContext>()
                        .UseSqlServer(configuration.GetConnectionString("BakeryDb"))
                        .UseLoggerFactory(LoggerFactory.Create(builder =>
                        {
                            builder.AddConsole();
                            builder.AddDebug();
                        }))
                        .EnableDetailedErrors()
                        .EnableSensitiveDataLogging()
                        .Options;
            });

            services.AddDbContext<BakeryDbContext>();

            return services;
        }

        public static IServiceCollection RegisterServices(this IServiceCollection services)
        {
            return services
               .AddScoped<IAuthService, AuthService>()
               .AddScoped<IDiscountCalculatorService, DiscountCalculatorService>()
               .AddScoped<IHashingService, HashingService>()
               .AddScoped<IIngredientsService, IngredientsService>()
               .AddScoped<IProductsOnSaleService, ProductsOnSaleService>()
               .AddScoped<IProductsService, ProductsService>();
        }    

        public static IServiceCollection RegisterRepositories(this IServiceCollection services)
        {
            return services
                    .AddScoped<IUsersRepository, UsersRepository>()
                   .AddScoped<IProductsRepository, ProductsRepository>()
                   .AddScoped<IIngredientsRepository, IngredientsRepository>()
                   .AddScoped<IProductsOnSaleRepository, ProductsOnSaleRepository>()
                   .AddScoped<IProductSaleRuleRepository, ProductSaleRuleRepository>();
        }
    }
}
