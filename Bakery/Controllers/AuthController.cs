﻿using Bakery.Common.Settings;
using Bakery.Domain.Services;
using Bakery.DTO.Request;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService authService;
        private readonly BakerySettings settings;

        public AuthController(IAuthService authService, IOptions<BakerySettings> options)
        {
            this.authService = authService;
            this.settings = options.Value;
        }

        [AllowAnonymous]
        [HttpPost("Token")]
        public async Task<IActionResult> Token([FromBody] AuthenticationRequest request, CancellationToken cancellationToken)
        {
            var credentialsValid = await authService.CheckCredentialsAsync(request , cancellationToken);

            if (credentialsValid)
            {
                var tokenString = GenerateJSONWebToken(request.Username);
                return Ok(new { token = tokenString });
            }


            return Unauthorized();
        }

        private string GenerateJSONWebToken(string username)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(settings.JwtKey));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            List<Claim> claims = new List<Claim>()
            {
                new Claim("username", username)
            };

            var token = new JwtSecurityToken(settings.JwtIssuer,
              settings.JwtIssuer,
              claims,
              expires: DateTime.Now.AddMinutes(60),
              signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
