﻿using Bakery.Domain.Services;
using Bakery.DTO.Request;
using Bakery.DTO.Response;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class IngredientsController : ControllerBase
    {
        private readonly IIngredientsService ingredientsService;

        public IngredientsController(IIngredientsService ingredientsService)
        {
            this.ingredientsService = ingredientsService;
        }

        public Task<PaginatedResponse<IngredientReponse>> Get([FromQuery] PaginatedRequest request, CancellationToken cancellationToken)
        {
            return ingredientsService.GetIngredientsAsync(request, cancellationToken);
        }
    }
}
