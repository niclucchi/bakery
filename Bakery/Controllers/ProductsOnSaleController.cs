﻿using Bakery.Domain.Services;
using Bakery.DTO.Request;
using Bakery.DTO.Response;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductsOnSaleController : ControllerBase
    {
        private readonly IProductsOnSaleService productsOnSaleService;

        public ProductsOnSaleController(IProductsOnSaleService productsOnSaleService)
        {
            this.productsOnSaleService = productsOnSaleService;
        }

        [HttpGet]
        [AllowAnonymous]
        public Task<PaginatedResponse<ProductOnSaleResponse>> Get([FromQuery] ProductsOnSalePaginatedRequest request, CancellationToken cancellationToken)
        {
            return productsOnSaleService.GetProductsOnSaleAsync(request, cancellationToken);
        }

        [HttpPost]
        public Task<int> Post([FromBody] CreateProductOnSaleRequest request, CancellationToken cancellationToken)
        {
            return productsOnSaleService.CreateProductOnSaleAsync(request, cancellationToken);
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] UpdateProductOnSaleRequest request, CancellationToken cancellationToken)
        {
            await productsOnSaleService.UpdateProductOnSaleAsync(request, cancellationToken);
            return Ok();
        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> Delete([FromRoute] int id, CancellationToken cancellationToken)
        {
            await productsOnSaleService.RemoveProductOnSaleAsync(id, cancellationToken);
            return Ok();
        }
    }
}
