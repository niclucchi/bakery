﻿using Bakery.Domain.Services;
using Bakery.DTO.Request;
using Bakery.DTO.Response;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Bakery.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly IProductsService productsService;

        public ProductsController(IProductsService productsService)
        {
            this.productsService = productsService;
        }

        [Route("{id}")]
        [HttpGet]
        public Task<ProductDetailResponse> Get([FromRoute]int id, CancellationToken cancellationToken)
        {
            return productsService.GetProductDetialAsync(id, cancellationToken);
        }

        [HttpGet]
        public Task<PaginatedResponse<ProductResponse>> Get([FromQuery]PaginatedRequest request, CancellationToken cancellationToken)
        {
            return productsService.GetProductsAsync(request, cancellationToken);
        }

        [HttpPost]
        public Task<int> Post([FromBody] CreateProductRequest request, CancellationToken cancellationToken)
        {
            return productsService.CreateProductAsync(request, cancellationToken);
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] UpdateProductRequest request, CancellationToken cancellationToken)
        {
            await productsService.UpdateProductAsync(request, cancellationToken);
            return Ok();
        }

        [Route("{id}")]
        [HttpDelete]
        public async Task<IActionResult> Delete([FromRoute]int id, CancellationToken cancellationToken)
        {
            await productsService.RemoveProductAsync(id, cancellationToken);
            return Ok();
        }
    }
}
