import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import {
  NgbModule,
  NgbModalConfig,
  NgbNavModule,
  NgbDropdownModule
} from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './components/nav-menu/nav-menu.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuard } from './guards/auth.guard';
import { RequestInterceptor } from './interceptors/request.interceptor';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ToastrModule } from 'ngx-toastr';
import { ProductsOnSaleService } from './services/products-on-sale.service';
import { OnSaleComponent } from './components/on-sale/on-sale.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProductsService } from './services/products.service';
import { ProductsListComponent } from './components/products-list/products-list.component';
import { UrlService } from './services/url.service';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { ConfirmModalComponent } from './components/confirmModal/confirm-modal.component';
import { ProductsDetailComponent } from './components/product-detail/products-detail.component';
import { IngredientsService } from './services/ingredients.service';
import { PutOnSaleComponent } from './components/put-on-sale/put-on-sale.component';
import { IngredientPickerComponent } from './components/ingredient-picker/ingrendient-picker.component';
import { ProductPickerComponent } from './components/product-picker/product-picker.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
	OnSaleComponent,
	LoginComponent,
	ProductsListComponent,
	ConfirmModalComponent,
	IngredientPickerComponent,
	ProductPickerComponent,
	ProductsDetailComponent,
	PutOnSaleComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
	NgxSpinnerModule,
	CurrencyMaskModule,
    NgbModule,
	BrowserAnimationsModule,
	NgxDatatableModule,
	NgbDropdownModule,
	NgbNavModule,
	ReactiveFormsModule,
	ToastrModule.forRoot(),
    RouterModule.forRoot([
    { path: '', component: OnSaleComponent, pathMatch: 'full'},
	{ path: 'products', component: ProductsListComponent, canActivate: [AuthGuard] },	
	{ path: 'products/:id', component: ProductsDetailComponent, canActivate: [AuthGuard] },	
	{ path: 'products/new', component: ProductsDetailComponent, canActivate: [AuthGuard] },	
	{ path: 'put-on-sale', component: PutOnSaleComponent, canActivate: [AuthGuard] },	
    { path: 'login', component: LoginComponent},
], { relativeLinkResolution: 'legacy' })
  ],
  providers: [
	AuthGuard,
    NgbModalConfig,
	ProductsOnSaleService,
	UrlService,
	IngredientsService,
	ProductsService,
	{ provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
