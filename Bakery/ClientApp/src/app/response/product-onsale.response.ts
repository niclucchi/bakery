import { Type } from 'class-transformer';

export class ProductOnSaleResponse {

	productOnSaleId:number;
	productId:number;
	productName:string;
	@Type(() => Date)
	putUpForSaleDateUtc : Date;
	fullPrice: number;
	discountedPrice?:number;
	quantity: number;
	totalFullprice: number;
	totalDiscountedPrice:number;
	daysFromPutOnSaleDate: number;
	isExpired: boolean;
}