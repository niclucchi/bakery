import { Type } from 'class-transformer';
import { ProductIngredientResponse } from './product-ingredient.response';
import { ProductResponse } from './product.response';

export class ProductDetailResponse extends ProductResponse {

	productIngredients : ProductIngredientResponse[] = [];
}