import { UnitOfMeasureEnum } from '../enums/unit-of-measure.enum';
import { IngredientResponse } from './ingredient.response';

export class ProductIngredientResponse  {

	productIngredientId:number;
	quantity:number;
	unitOfMeasure: UnitOfMeasureEnum;
	ingredient: IngredientResponse;

}