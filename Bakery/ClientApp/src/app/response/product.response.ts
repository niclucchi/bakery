import { Type } from 'class-transformer';

export class ProductResponse {

	productId:number;
	productName:string;
	price: number;
}