import { SortDirectionEnum } from "../components/datatable/sort-direction.enum";


export class PaginatedResponse<T> {
    currentPage: number;
    pageSize: number;
	numberOfPages: string;
	totalRecords: SortDirectionEnum;
	data: Array<T>;
}