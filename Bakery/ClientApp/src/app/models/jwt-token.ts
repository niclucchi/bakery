export class JwtToken {

	public expirationDate: Date;
	public aud: string;
	public cid: string;
	public iss: string;
	public uid: string;


	constructor(token: {
		aud:string,
		cid:string,
		exp:number,
		iss:string,
		uid:string
	}) {
		this.aud = token.aud;
		this.cid =  token.cid;
		this.iss =  token.iss;
		this.uid =  token.uid;
		this.expirationDate = new Date(token.exp * 1000);
	}

	isExpired(){
		return this.expirationDate < new Date();
	}

}