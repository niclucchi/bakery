import { HttpClient } from "@angular/common/http";
import { CallTypeEnum } from "../enums/call-type.enum";
import { PaginatedRequest } from "../request/paginated-request";
import { PaginatedResponse } from "../response/paginated-response";
import { UrlBuilderService } from "./url-builder.service";
import { plainToClass } from 'class-transformer';
import { Injectable } from "@angular/core";
import { IngredientResponse } from "../response/ingredient.response";

@Injectable()
export class IngredientsService {


	constructor(
		private urlBuilderService: UrlBuilderService,
		private httpClient: HttpClient) {

	}

	async getIngredients(paginatedRequest: PaginatedRequest): Promise<PaginatedResponse<IngredientResponse>> {

		const url = this.urlBuilderService.getUrlByCallType(CallTypeEnum.GetIngredients);
		let httpParams = paginatedRequest.ToHttpParams();		

		
		var resp = await this.httpClient.get<PaginatedResponse<IngredientResponse>>(url, { params: httpParams }).toPromise();

		resp.data = plainToClass(IngredientResponse, resp.data);

		return resp;
	}
}