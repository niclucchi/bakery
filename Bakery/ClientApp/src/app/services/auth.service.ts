import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CallTypeEnum } from '../enums/call-type.enum';
import { JwtTokenRequest } from '../request/jwt-token.request';
import { JwtTokenResponse } from '../response/jwt-token.response';
import { LocalStorageService } from './local-storage.service';
import { UrlBuilderService } from './url-builder.service';


@Injectable({
    providedIn: 'root'
})

export class AuthService {

    constructor(
        private urlBuilderService: UrlBuilderService, 
        private httpClient: HttpClient) {

    }

    async getUserToken(username: string, password: string): Promise<JwtTokenResponse> {
        
		const url = this.urlBuilderService.getUrlByCallType(CallTypeEnum.Token);

		const request = new JwtTokenRequest();
		request.password = password;
		request.username = username;

		return await this.httpClient.post<JwtTokenResponse>(url, request).toPromise();

    }

}
