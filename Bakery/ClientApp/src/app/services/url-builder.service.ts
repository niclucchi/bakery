import { Injectable } from '@angular/core';
import { environment } from '@env';
import { CallTypeEnum } from '../enums/call-type.enum';


@Injectable({
	providedIn: 'root'
})

export class UrlBuilderService {

	public baseAddrApplicationServer: string;

	constructor() {
		this.baseAddrApplicationServer = environment.baseAddrApplicationServer;
	}

	getUrlByCallType(callType: CallTypeEnum): string {
		let result: string;

		switch (callType) {

			case CallTypeEnum.Token:
				result = this.baseAddrApplicationServer + '/Auth/Token';
				break;
			case CallTypeEnum.GetProductsOnSale:
				result = this.baseAddrApplicationServer + '/ProductsOnSale';
				break;
			case CallTypeEnum.GetProducts:
				result = this.baseAddrApplicationServer + '/Products';
				break;
			case CallTypeEnum.GetIngredients:
				result = this.baseAddrApplicationServer + '/Ingredients';
				break;
			default:
				throw new Error(`CallTypeEnum ${callType} non gestito.`);
				break;
		}

		return result;
	}

}
