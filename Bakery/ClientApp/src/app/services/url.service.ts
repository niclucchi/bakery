import { ActivatedRoute } from '@angular/router';
import { Injectable } from '@angular/core';
import { plainToClass } from 'class-transformer';


@Injectable()
export class UrlService {

    constructor() { }

    parseIntRouteParameter(route: ActivatedRoute, paramName: string): number {

        const parameter: string = route.snapshot.paramMap.get(paramName);

        if (parameter && Number.isInteger(Number(parameter))) {
            return Number.parseInt(parameter);
        }

        return null;
    }

    parseIntQsParameter(route: ActivatedRoute,paramName: string): number {

        const parameter: string = route.snapshot.queryParamMap.get(paramName);

        if (parameter && Number.isInteger(Number(parameter))) {
            return Number.parseInt(parameter);
        }

        return null;
    }

    getParam(route: ActivatedRoute, paramName: string): string {
        const parameter: string = route.snapshot.queryParamMap.get(paramName);
        return parameter;
    }
}


