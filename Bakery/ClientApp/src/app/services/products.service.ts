import { HttpClient } from "@angular/common/http";
import { CallTypeEnum } from "../enums/call-type.enum";
import { PaginatedRequest } from "../request/paginated-request";
import { PaginatedResponse } from "../response/paginated-response";
import { ProductOnSaleResponse } from "../response/product-onsale.response";
import { UrlBuilderService } from "./url-builder.service";
import { plainToClass } from 'class-transformer';
import { Injectable } from "@angular/core";
import { ProductResponse } from "../response/product.response";
import { ProductDetailResponse } from "../response/product-detail.response";
import { CreateProductRequest } from "../request/create-product.request";
import { UpdateProductRequest } from "../request/update-product.request";

@Injectable()
export class ProductsService {


	constructor(
		private urlBuilderService: UrlBuilderService,
		private httpClient: HttpClient) {

	}

	async getProducts(paginatedRequest: PaginatedRequest): Promise<PaginatedResponse<ProductResponse>> {

		const url = this.urlBuilderService.getUrlByCallType(CallTypeEnum.GetProducts);
		let httpParams = paginatedRequest.ToHttpParams();		

		
		var resp = await this.httpClient.get<PaginatedResponse<ProductResponse>>(url, { params: httpParams }).toPromise();

		resp.data = plainToClass(ProductResponse, resp.data);

		return resp;
	}

	async getProductDetail(productId: number): Promise<ProductDetailResponse> {

		const url = this.urlBuilderService.getUrlByCallType(CallTypeEnum.GetProducts);
				
		var resp = await this.httpClient.get<ProductDetailResponse>(url + "/" + productId ).toPromise();

		return plainToClass(ProductDetailResponse, resp);
	}

	async createProduct(request: CreateProductRequest): Promise<number> {

		const url = this.urlBuilderService.getUrlByCallType(CallTypeEnum.GetProducts);
				
		return await this.httpClient.post<number>(url, request).toPromise();
	}

	async updateProduct(request: UpdateProductRequest): Promise<any> {

		const url = this.urlBuilderService.getUrlByCallType(CallTypeEnum.GetProducts);
				
		return await this.httpClient.put<any>(url, request).toPromise();
	}

	async deleteProduct(productId: number): Promise<any> {

		const url = this.urlBuilderService.getUrlByCallType(CallTypeEnum.GetProducts);
				
		return await this.httpClient.delete<PaginatedResponse<ProductResponse>>(url + "/" + productId ).toPromise();
	}

}