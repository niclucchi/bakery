import { Observable, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { plainToClass } from 'class-transformer';
import { JwtToken } from '../models/jwt-token';
import jwt_decode from 'jwt-decode';


@Injectable({
    providedIn: 'root'
})
export class LocalStorageService {

    public readonly JWT_TOKEN_KEY: string = 'JWT_TOKEN_KEY';
	public readonly USERNAME_KEY: string = 'USERNAME_KEY';

    constructor() { }

    public get(key: string): any {
        const stringData = localStorage.getItem(key);
        if (stringData == null)
            return null;
        else
            return JSON.parse(stringData);
    }

    private set(key: string, obj: any) {
        const stringData = JSON.stringify(obj);
        localStorage.setItem(key, stringData);
    }

  
    removeJwtToken() {
        localStorage.removeItem(this.JWT_TOKEN_KEY);
    }


	removeUsername() {
        localStorage.removeItem(this.USERNAME_KEY);
    }

	getDecodedJwtToken(): any {
		const token = this.getJwtToken();
		
		if(token)
			return new JwtToken(jwt_decode(token));
		
		return null;
	}

    getJwtToken(): string {
        return this.get(this.JWT_TOKEN_KEY);
    }

    setJwtToken(token: string) {
        this.set(this.JWT_TOKEN_KEY, token);
    }

	getUsername(): string {
        return this.get(this.USERNAME_KEY);
    }

    setUsername(token: string) {
        this.set(this.USERNAME_KEY, token);
    }


    public removeAll() {
        this.removeJwtToken();
		this.removeUsername();
    }
}
