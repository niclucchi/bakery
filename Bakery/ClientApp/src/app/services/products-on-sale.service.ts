import { HttpClient } from "@angular/common/http";
import { CallTypeEnum } from "../enums/call-type.enum";
import { PaginatedRequest } from "../request/paginated-request";
import { PaginatedResponse } from "../response/paginated-response";
import { ProductOnSaleResponse } from "../response/product-onsale.response";
import { UrlBuilderService } from "./url-builder.service";
import { plainToClass } from 'class-transformer';
import { Injectable } from "@angular/core";
import { UpdateProductOnSaleRequest } from "../request/update-product-on-sale.request";
import { CreateProductOnSaleRequest } from "../request/create-product-on-sale.request";

@Injectable()
export class ProductsOnSaleService {


	constructor(
		private urlBuilderService: UrlBuilderService,
		private httpClient: HttpClient) {

	}

	async getProductsOnSale(paginatedRequest: PaginatedRequest, hideExpiredProducts: boolean): Promise<PaginatedResponse<ProductOnSaleResponse>> {

		const url = this.urlBuilderService.getUrlByCallType(CallTypeEnum.GetProductsOnSale);
		let httpParams = paginatedRequest.ToHttpParams();		
		httpParams = httpParams.append("hideExpiredProducts", hideExpiredProducts);
		
		var resp = await this.httpClient.get<PaginatedResponse<ProductOnSaleResponse>>(url, { params: httpParams }).toPromise();

		resp.data = plainToClass(ProductOnSaleResponse, resp.data);

		return resp;
	}

	async getProductOnSale(productOnSaleId: number): Promise<ProductOnSaleResponse> {

		const url = this.urlBuilderService.getUrlByCallType(CallTypeEnum.GetProductsOnSale);
			
		var resp = await this.httpClient.get<ProductOnSaleResponse>(url + "/" + productOnSaleId).toPromise();

		return plainToClass(ProductOnSaleResponse, resp);
	}

	async createProductOnSale(request: CreateProductOnSaleRequest): Promise<number> {

		const url = this.urlBuilderService.getUrlByCallType(CallTypeEnum.GetProductsOnSale);
				
		return await this.httpClient.post<number>(url, request).toPromise();
	}

	async updateProductOnSale(request: UpdateProductOnSaleRequest): Promise<any> {

		const url = this.urlBuilderService.getUrlByCallType(CallTypeEnum.GetProductsOnSale);
				
		return await this.httpClient.put<any>(url, request).toPromise();
	}

	async deleteProductOnSale(productOnSaleId: number): Promise<any> {

		const url = this.urlBuilderService.getUrlByCallType(CallTypeEnum.GetProductsOnSale);
				
		return await this.httpClient.delete<any>(url + "/" + productOnSaleId ).toPromise();
	}

}