export enum UnitOfMeasureEnum {
	Mg = 1,
	Gr = 2,
	Hg = 3,
	Kg = 4,
	Ml = 5,
	Cl = 6,
	Dl = 7,
	L = 8,
}
