export enum CallTypeEnum {
    Token,
	GetProductsOnSale,
	GetProducts,
	GetIngredients,
}
