import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LocalStorageService } from '../services/local-storage.service';


@Injectable({
    providedIn: 'root'
})
export class RequestInterceptor implements HttpInterceptor {
    constructor(
        private localStorageService: LocalStorageService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        let headers = req.headers;
		const accessToken = this.localStorageService.getJwtToken();

        if (accessToken) {
            const bearerToken = `Bearer ${accessToken}`;
            headers = headers.set('Authorization', bearerToken);
        }
		
        const authReq = req.clone({ headers: headers });
        return next.handle(authReq) as any;
    }
}
