
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LocalStorageService } from '../services/local-storage.service';
import { ToastrService } from 'ngx-toastr';


@Injectable({
	providedIn: 'root'
})
export class AuthGuard implements CanActivate {
	constructor(private localStorageService: LocalStorageService, private router: Router, private toastrService: ToastrService) {
	}

	canActivate(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

		const token = this.localStorageService.getDecodedJwtToken();
	
		if (!token || token.isExpired()) {
			this.router.navigate(['/login'], { queryParams: { returnUrl: window.location.pathname + window.location.search } });
			return false;
		}

		return true;
	}
}
