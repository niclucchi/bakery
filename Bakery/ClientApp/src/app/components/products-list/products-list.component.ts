import { Component, OnInit, Renderer2 } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PaginatedRequest } from 'src/app/request/paginated-request';
import { PaginatedResponse } from 'src/app/response/paginated-response';
import { ProductOnSaleResponse } from 'src/app/response/product-onsale.response';
import { ProductResponse } from 'src/app/response/product.response';
import { AuthService } from 'src/app/services/auth.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { ProductsOnSaleService } from 'src/app/services/products-on-sale.service';
import { ProductsService } from 'src/app/services/products.service';
import { ConfirmModalComponent } from '../confirmModal/confirm-modal.component';
import { DatatableBaseComponent } from '../datatable/datatable-base.component';

@Component({
	selector: 'app-products-list-component',
	templateUrl: './products-list.component.html'
})
export class ProductsListComponent extends DatatableBaseComponent<ProductResponse> implements OnInit   {

	public searchText = "";

	constructor(private toastr: ToastrService,
		private router: Router,		
		private route: ActivatedRoute,
		private modalService: NgbModal,
		private localStorageService: LocalStorageService,
		private spinner: NgxSpinnerService,
		private productsService: ProductsService) {
			super(toastr);
            this.sortColumnString = "ProductName";
    }

	async ngOnInit() {
        await this.refreshDataTable();
    }

    async retrieveData(paginatedRequest: PaginatedRequest): Promise<PaginatedResponse<ProductResponse>> {

		try {
			this.spinner.show();			
			paginatedRequest.search = this.searchText;
			return await this.productsService.getProducts(paginatedRequest);
		} catch {
			await this.toastr.error("Errore imprevisto.");
		} finally{
			this.spinner.hide();
		}
    }

	async deleteProduct(productId: number){

		const modalRef = this.modalService.open(ConfirmModalComponent);
		const modale = modalRef.componentInstance as ConfirmModalComponent;
		modale.title = "Attenzione.";
		modale.message = "Confermi di voler eliminare il prodotto dall'anagrafica?";
		modalRef.result.then(
			async () => { // Close()	
				
				this.spinner.show();			

				try
				{
					await this.productsService.deleteProduct(productId);		
					this.toastr.success("Prodotto eliminato")
					this.refreshDataTable();
				}
				catch
				{
					this.toastr.error("Errore imprevisto")
				}
				finally
				{
					this.spinner.hide();			
				}	
			},
			async () => {
				
			}
		);
	}

}
