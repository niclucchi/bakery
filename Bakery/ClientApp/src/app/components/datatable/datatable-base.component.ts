
import { Component } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { PaginatedRequest } from 'src/app/request/paginated-request';
import { PaginatedResponse } from 'src/app/response/paginated-response';
import { IPageInfo } from './page-info';
import { SortDirectionEnum } from './sort-direction.enum';
import { ISortInfo } from './sort.info';


@Component({template:''})

export abstract class DatatableBaseComponent<TResponse> {

    public isLoading: boolean;
    public currentPage: number = 1;
    public pageSize: number = 20;
    public pageNum: number = 1;
    public sortDirectionString: string;
    public sortColumnString: string;
    public data: PaginatedResponse<TResponse>;

    constructor(public toastrService: ToastrService) {

    }

    abstract retrieveData(paginatedRequest: PaginatedRequest) : Promise<PaginatedResponse<TResponse>>;
    
    async refreshDataTable() : Promise<any>{
        this.isLoading = true;

        try {
            let paginatedRequest = this.getPaginatedRequest();
            this.data = await this.retrieveData(paginatedRequest);
        }
        catch {
            this.toastrService.error("Errore imprevisto durante il caricamento dei dati.");
            
        } finally {
            this.isLoading = false;
        }
    }

    async onPageChangeAsync(pageInfo: IPageInfo): Promise<any> {
        this.pageNum = pageInfo.offset + 1;
        await this.refreshDataTable();
    }

    async onSortChangeAsync(sortInfo: ISortInfo): Promise<any> {
        this.sortDirectionString = sortInfo.sorts[0].dir;
        this.sortColumnString = sortInfo.sorts[0].prop;
        this.pageNum = 1;
        await this.refreshDataTable();
    }

    getPaginatedRequest(): PaginatedRequest {

        const request = new PaginatedRequest();
        request.pageNum = this.pageNum;
        request.pageSize = this.pageSize;
        request.sortDirection = this.sortDirectionString ? SortDirectionEnum[this.sortDirectionString.toUpperCase()] : SortDirectionEnum.ASC;
        request.sortBy = this.sortColumnString;
        return request;
    }
}
