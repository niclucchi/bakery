export interface ISortInfo {
    sorts: {
        dir: string,
        prop: string
    }[];
    column: {};
    prevValue: string;
    newValue: string;
}
