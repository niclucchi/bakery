import { Route } from '@angular/compiler/src/core';
import { Component, OnInit, Renderer2, Inject, Input, ElementRef, ViewChild } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';

@Component({
	templateUrl: 'confirm-modal.component.html',
	styleUrls: ['./confirm-modal.component.css']
})
export class ConfirmModalComponent {

	public isReady: boolean;
	@Input() title: string;
	@Input() message: string;

	constructor(
		public activeModal: NgbActiveModal) {
	}

	async confirm(): Promise<any> {
		this.activeModal.close(true);
	}

	closeModal() {
		this.activeModal.dismiss();
	}
}
