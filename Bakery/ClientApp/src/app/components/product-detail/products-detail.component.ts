import { Component, OnInit, Renderer2, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { UnitOfMeasureEnum } from 'src/app/enums/unit-of-measure.enum';
import { CreateProductRequest } from 'src/app/request/create-product.request';
import { PaginatedRequest } from 'src/app/request/paginated-request';
import { ProductIngredientRequest } from 'src/app/request/product-ingredient.request';
import { UpdateProductRequest } from 'src/app/request/update-product.request';
import { IngredientResponse } from 'src/app/response/ingredient.response';
import { PaginatedResponse } from 'src/app/response/paginated-response';
import { ProductDetailResponse } from 'src/app/response/product-detail.response';
import { ProductIngredientResponse } from 'src/app/response/product-ingredient.response';
import { ProductOnSaleResponse } from 'src/app/response/product-onsale.response';
import { ProductResponse } from 'src/app/response/product.response';
import { AuthService } from 'src/app/services/auth.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { ProductsOnSaleService } from 'src/app/services/products-on-sale.service';
import { ProductsService } from 'src/app/services/products.service';
import { UrlService } from 'src/app/services/url.service';
import { ConfirmModalComponent } from '../confirmModal/confirm-modal.component';
import { DatatableBaseComponent } from '../datatable/datatable-base.component';
import { IngredientPickerComponent } from '../ingredient-picker/ingrendient-picker.component';

@Component({
	selector: 'app-product-detail-component',
	templateUrl: './products-detail.component.html'
})
export class ProductsDetailComponent implements OnInit   {

	public searchText = "";
	public product = new ProductDetailResponse();
	get unitsOfMeasure() {return UnitOfMeasureEnum;}

	@ViewChild('productForm') public productForm: NgForm;

	constructor(private toastr: ToastrService,
		private router: Router,		
		private modalService: NgbModal,
		private route: ActivatedRoute,
		private localStorageService: LocalStorageService,
		private urlService: UrlService,
		private spinner: NgxSpinnerService,
		private productsService: ProductsService) {
    }

	async ngOnInit() {
        var productId = this.urlService.parseIntRouteParameter(this.route, "id");
		if(productId)
			await this.loadProduct(productId);
    }

    async loadProduct(productId:number): Promise<any> {
		
		try {
			this.spinner.show();			
			this.product = await this.productsService.getProductDetail(productId);
		} catch {
			await this.toastr.error("Errore imprevisto.");
		} finally{
			this.spinner.hide();
		}
    }

	openIngredientPicker(){
		const modalRef = this.modalService.open(IngredientPickerComponent, { size: "lg" });
		const modale = modalRef.componentInstance as IngredientPickerComponent;
		modale.disabledIngredients = this.product.productIngredients.map(x => x.ingredient.ingredientId);

		modalRef.result.then(
			async () => { 
				
			},
			async () => {
				if (modale.selectedIngredient) {
					var productIngredient = new ProductIngredientResponse();
					productIngredient.ingredient = new IngredientResponse();
					productIngredient.ingredient.ingredientId = modale.selectedIngredient.ingredientId;
					productIngredient.ingredient.name  = modale.selectedIngredient.name;
					productIngredient.quantity = 0;
					productIngredient.unitOfMeasure = UnitOfMeasureEnum.Gr;
					this.product.productIngredients.push(productIngredient);
				}
			}
		);
	}

	removeIngredient(productIngredient: ProductIngredientResponse)
	{
		this.product.productIngredients = this.product.productIngredients.filter(x => x != productIngredient);
	}

	async onSaveClick(){
		if(this.productForm.valid){
			this.spinner.show();			

			try
			{
				if(this.product.productId && this.product.productId > 0) {
					await this.updateProduct();
				} else {
					await this.createProduct();
				}
				this.toastr.success("Prodotto salvato correttamente")
			}
			catch
			{
				this.toastr.error("Errore imprevisto")
			}
			finally
			{
				this.spinner.hide();			
			}

		}
	}


	async createProduct(){
		const request = new CreateProductRequest();
		request.productName = this.product.productName;
		request.price = this.product.price;
		
		request.productIngredients = this.product.productIngredients.map(x => {
			var ingredient = new ProductIngredientRequest();
			ingredient.ingredientId = x.ingredient.ingredientId;
			ingredient.quantity = x.quantity;
			ingredient.unitOfMeasure = +x.unitOfMeasure;
			return ingredient;
		})

		var productId = await this.productsService.createProduct(request);
		this.router.navigate(["/products/" + productId]);
	}
	
	async updateProduct() {
		const request = new UpdateProductRequest();
		request.productId = this.product.productId;
		request.productName = this.product.productName;
		request.price = this.product.price;
		
		request.productIngredients = this.product.productIngredients.map(x => {
			var ingredient = new ProductIngredientRequest();
			ingredient.ingredientId = x.ingredient.ingredientId;
			ingredient.quantity = x.quantity;
			ingredient.unitOfMeasure = +x.unitOfMeasure;
			return ingredient;
		})

		await this.productsService.updateProduct(request);
	}
}
