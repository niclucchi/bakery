import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/services/local-storage.service';

@Component({
	selector: 'app-nav-menu',
	templateUrl: './nav-menu.component.html',
	styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {
	isExpanded = false;
	isLogged = false;
	username = "";
	constructor(private localStorageService: LocalStorageService, private router: Router) {

	}

	ngOnInit() : void {
		const token = this.localStorageService.getDecodedJwtToken();
		const username = this.localStorageService.getUsername();
		if(token)
			this.isLogged = true;
		if(username)
			this.username = username;
	}

	collapse() {
		this.isExpanded = false;
	}

	toggle() {
		this.isExpanded = !this.isExpanded;
	}

	logout(){
		this.localStorageService.removeAll();
		this.router.navigate(["/login"]);
		this.isLogged = false;
		this.username="";
	}
}
