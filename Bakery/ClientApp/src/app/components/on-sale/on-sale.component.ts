import { Component, OnInit, Renderer2 } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { PaginatedRequest } from 'src/app/request/paginated-request';
import { PaginatedResponse } from 'src/app/response/paginated-response';
import { ProductOnSaleResponse } from 'src/app/response/product-onsale.response';
import { AuthService } from 'src/app/services/auth.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { ProductsOnSaleService } from 'src/app/services/products-on-sale.service';
import { DatatableBaseComponent } from '../datatable/datatable-base.component';

@Component({
	selector: 'app-onsale-component',
	templateUrl: './on-sale.component.html'
})
export class OnSaleComponent extends DatatableBaseComponent<ProductOnSaleResponse> implements OnInit   {

	public searchText = "";

	constructor(private toastr: ToastrService,
		private router: Router,		
		private route: ActivatedRoute,
		private localStorageService: LocalStorageService,
		private spinner: NgxSpinnerService,
		private productsOnSaleService: ProductsOnSaleService) {
			super(toastr);
            this.sortColumnString = "Product.ProductName";
    }

	async ngOnInit() {
        await this.refreshDataTable();
    }

    async retrieveData(paginatedRequest: PaginatedRequest): Promise<PaginatedResponse<ProductOnSaleResponse>> {

		try {
			this.spinner.show();			
			paginatedRequest.search = this.searchText;
			return await this.productsOnSaleService.getProductsOnSale(paginatedRequest, true);
		} catch {
			await this.toastr.error("Errore imprevisto.");
		} finally{
			this.spinner.hide();
		}
    }

}
