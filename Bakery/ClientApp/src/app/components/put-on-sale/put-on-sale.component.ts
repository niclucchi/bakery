import { Component, OnInit, Renderer2 } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CreateProductOnSaleRequest } from 'src/app/request/create-product-on-sale.request';
import { PaginatedRequest } from 'src/app/request/paginated-request';
import { PaginatedResponse } from 'src/app/response/paginated-response';
import { ProductOnSaleResponse } from 'src/app/response/product-onsale.response';
import { ProductResponse } from 'src/app/response/product.response';
import { AuthService } from 'src/app/services/auth.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { ProductsOnSaleService } from 'src/app/services/products-on-sale.service';
import { ConfirmModalComponent } from '../confirmModal/confirm-modal.component';
import { DatatableBaseComponent } from '../datatable/datatable-base.component';
import { ProductPickerComponent } from '../product-picker/product-picker.component';

@Component({
	selector: 'app-put-on-sale-component',
	templateUrl: './put-on-sale.component.html'
})
export class PutOnSaleComponent extends DatatableBaseComponent<ProductOnSaleResponse> implements OnInit   {

	public searchText : string = "";
	public quantity : number = 0;
	public selectedProduct : ProductResponse;

	constructor(private toastr: ToastrService,
		private router: Router,		
		private route: ActivatedRoute,
		private modalService:NgbModal,
		private localStorageService: LocalStorageService,
		private spinner: NgxSpinnerService,
		private productsOnSaleService: ProductsOnSaleService) {
			super(toastr);
            this.sortColumnString = "Product.ProductName";
    }

	async ngOnInit() {
        await this.refreshDataTable();
    }

    async retrieveData(paginatedRequest: PaginatedRequest): Promise<PaginatedResponse<ProductOnSaleResponse>> {

		try {
			this.spinner.show();			
			paginatedRequest.search = this.searchText;
			return await this.productsOnSaleService.getProductsOnSale(paginatedRequest, false);
		} catch {
			await this.toastr.error("Errore imprevisto.");
		} finally{
			this.spinner.hide();
		}
    }


	async deleteProductOnSale(productOnSaleId: number){

		const modalRef = this.modalService.open(ConfirmModalComponent);
		const modale = modalRef.componentInstance as ConfirmModalComponent;
		modale.title = "Attenzione.";
		modale.message = "Confermi di voler rimuovere il prodotto dalla vendita?";
		modalRef.result.then(
			async () => { // Close()	
				
				this.spinner.show();			

				try
				{
					await this.productsOnSaleService.deleteProductOnSale(productOnSaleId);		
					this.toastr.success("Prodotto rimosso dalla vendita")
					this.refreshDataTable();
				}
				catch
				{
					this.toastr.error("Errore imprevisto")
				}
				finally
				{
					this.spinner.hide();			
				}	
			},
			async () => {
				
			}
		);
	}

	openProductPicker(){
		const modalRef = this.modalService.open(ProductPickerComponent, { size: "lg" });
		const modale = modalRef.componentInstance as ProductPickerComponent;
		
		modalRef.result.then(
			async () => { 
				
			},
			async () => {
				if (modale.selectedProduct) {
					this.selectedProduct = modale.selectedProduct;					
				}
			}
		);
	}

	async createProductOnSale(){

		try {
			this.spinner.show();			
			const request = new CreateProductOnSaleRequest();
			request.quantity = +this.quantity;
			request.productId = this.selectedProduct.productId;
			await this.productsOnSaleService.createProductOnSale(request);
			await this.toastrService.success("Prodotto aggiunto alla vendita");
			await this.refreshDataTable();
		} catch {
			await this.toastr.error("Errore imprevisto.");
		} finally{
			this.spinner.hide();
		}
		
	}
}
