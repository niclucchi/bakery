import { Component, Input, OnInit, Renderer2, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { UnitOfMeasureEnum } from 'src/app/enums/unit-of-measure.enum';
import { CreateProductRequest } from 'src/app/request/create-product.request';
import { PaginatedRequest } from 'src/app/request/paginated-request';
import { ProductIngredientRequest } from 'src/app/request/product-ingredient.request';
import { UpdateProductRequest } from 'src/app/request/update-product.request';
import { IngredientResponse } from 'src/app/response/ingredient.response';
import { PaginatedResponse } from 'src/app/response/paginated-response';
import { ProductDetailResponse } from 'src/app/response/product-detail.response';
import { ProductIngredientResponse } from 'src/app/response/product-ingredient.response';
import { ProductOnSaleResponse } from 'src/app/response/product-onsale.response';
import { ProductResponse } from 'src/app/response/product.response';
import { AuthService } from 'src/app/services/auth.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';
import { ProductsOnSaleService } from 'src/app/services/products-on-sale.service';
import { ProductsService } from 'src/app/services/products.service';
import { UrlService } from 'src/app/services/url.service';
import { ConfirmModalComponent } from '../confirmModal/confirm-modal.component';
import { DatatableBaseComponent } from '../datatable/datatable-base.component';
import { IngredientPickerComponent } from '../ingredient-picker/ingrendient-picker.component';

@Component({
	selector: 'app-put-on-sale-modal-component',
	templateUrl: './put-on-sale-modal.component.html'
})
export class PutOnSaleModalComponent implements OnInit   {

	public searchText = "";
	public product = new ProductOnSaleResponse();
	get unitsOfMeasure() {return UnitOfMeasureEnum;}

	@Input() productOnSaleId?: number;
	@ViewChild('productForm') public productForm: NgForm;

	constructor(private toastr: ToastrService,
		private router: Router,		
		private modalService: NgbModal,
		private route: ActivatedRoute,
		private localStorageService: LocalStorageService,
		private urlService: UrlService,
		private spinner: NgxSpinnerService,
		private productsOnSaleService: ProductsOnSaleService) {
    }

	async ngOnInit() {
        
		if(this.productOnSaleId)
			await this.loadProduct(this.productOnSaleId);
    }

    async loadProduct(productOnSaleId:number): Promise<any> {
		
		try {
			this.spinner.show();			
			this.product = await this.productsOnSaleService.getProductOnSale(productOnSaleId);
		} catch {
			await this.toastr.error("Errore imprevisto.");
		} finally{
			this.spinner.hide();
		}
    }


	async onSaveClick(){
		if(this.productForm.valid){
			this.spinner.show();			

			try
			{
				if(this.product.productId && this.product.productId > 0) {
				
				} else {
					
				}
				this.toastr.success("Prodotto salvato correttamente")
			}
			catch
			{
				this.toastr.error("Errore imprevisto")
			}
			finally
			{
				this.spinner.hide();			
			}

		}
	}
}
