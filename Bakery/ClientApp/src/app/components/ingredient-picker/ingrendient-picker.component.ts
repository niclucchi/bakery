import { Component, OnInit, Renderer2, Inject, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { IngredientResponse } from 'src/app/response/ingredient.response';
import { DatatableBaseComponent } from '../datatable/datatable-base.component';
import { IngredientsService } from 'src/app/services/ingredients.service';
import { PaginatedResponse } from 'src/app/response/paginated-response';
import { PaginatedRequest } from 'src/app/request/paginated-request';

@Component({
    templateUrl: './ingrendient-picker.component.html'
})
export class IngredientPickerComponent extends DatatableBaseComponent<IngredientResponse> implements OnInit  {

	public isReady: boolean;
	public isLoading: boolean;
	public searchText: string;
	public selectedIngredient: IngredientResponse;
	@Input() public disabledIngredients: number[] = [];
	
	constructor(
		public activeModal: NgbActiveModal,
		private ingredientsService: IngredientsService, 
		toastrService: ToastrService,	
		private spinner: NgxSpinnerService) {
        super(toastrService);
        this.sortColumnString = "name";
    }

	async ngOnInit() {
        await this.refreshDataTable();
    }

    async retrieveData(paginatedRequest: PaginatedRequest): Promise<PaginatedResponse<IngredientResponse>> {
		paginatedRequest.search = this.searchText;
        return await this.ingredientsService.getIngredients(paginatedRequest);
    }

	async onIngredientPicked(ingredient:IngredientResponse){
		this.selectedIngredient = ingredient;
		this.closeModal();
	}

    closeModal() {
        this.activeModal.dismiss();
    }

	isIngredientDisabled(ingredient: IngredientResponse){
		var disabledIngredients = this
			.disabledIngredients
			.filter(x => x == ingredient.ingredientId);
			
		return disabledIngredients.length > 0;
	}

}
