import { Component, Renderer2 } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from 'src/app/services/auth.service';
import { LocalStorageService } from 'src/app/services/local-storage.service';

@Component({
	selector: 'app-login-component',
	templateUrl: './login.component.html'
})
export class LoginComponent {
	loginForm: FormGroup;
	loading = false;
	submitted = false;
	returnUrl: string;
	isPageLoaded = false;

	constructor(
		private formBuilder: FormBuilder,
		private route: ActivatedRoute,
		private router: Router,
		private toastrSerive: ToastrService,
		private authService: AuthService,
		private spinner: NgxSpinnerService,
		private localStorageService: LocalStorageService
	) {
		this.route.queryParams.subscribe(params => {
			this.returnUrl = params['returnUrl'];
		});
	}

	ngOnInit() {
		this.loginForm = this.formBuilder.group({
			username: ['', Validators.required],
			password: ['', Validators.required],
			rememberMe: false
		});

		this.isPageLoaded = true;
	}

	// convenience getter for easy access to form fields
	get f() {
		return this.loginForm.controls;
	}

	async tryLogin(): Promise<any> {
		
		this.spinner.show();

		// stop here if form is invalid
		if (this.loginForm.invalid) {
			return;
		}

		try {
			this.localStorageService.removeAll();

			const tokenResponse = await this.authService.getUserToken(this.f.username.value, this.f.password.value);
			this.localStorageService.setJwtToken(tokenResponse.token);
			this.localStorageService.setUsername(this.f.username.value);

			if (this.returnUrl) {
				window.location.href = this.returnUrl;
			} else {
				window.location.href = "/products";
			}
		}
		catch
		{

			this.localStorageService.removeAll();
			this.toastrSerive.error("Username o password errati.")
		}
		finally {
			this.spinner.hide();
		}
	}
}
