import { Component, OnInit, Renderer2, Inject, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { IngredientResponse } from 'src/app/response/ingredient.response';
import { DatatableBaseComponent } from '../datatable/datatable-base.component';
import { IngredientsService } from 'src/app/services/ingredients.service';
import { PaginatedResponse } from 'src/app/response/paginated-response';
import { PaginatedRequest } from 'src/app/request/paginated-request';
import { ProductResponse } from 'src/app/response/product.response';
import { ProductsService } from 'src/app/services/products.service';

@Component({
    templateUrl: './product-picker.component.html'
})
export class ProductPickerComponent extends DatatableBaseComponent<ProductResponse> implements OnInit  {

	public isReady: boolean;
	public isLoading: boolean;
	public searchText: string;
	public selectedProduct: ProductResponse;
	
	constructor(
		public activeModal: NgbActiveModal,
		private productsService: ProductsService, 
		toastrService: ToastrService,	
		private spinner: NgxSpinnerService) {
        super(toastrService);
        this.sortColumnString = "productName";
    }

	async ngOnInit() {
        await this.refreshDataTable();
    }

    async retrieveData(paginatedRequest: PaginatedRequest): Promise<PaginatedResponse<ProductResponse>> {
		paginatedRequest.search = this.searchText;
        return await this.productsService.getProducts(paginatedRequest);
    }

	async onProductPicked(product:ProductResponse){
		this.selectedProduct = product;
		this.closeModal();
	}

    closeModal() {
        this.activeModal.dismiss();
    }
	
}
