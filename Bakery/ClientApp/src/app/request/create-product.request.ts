import { ProductIngredientRequest } from './product-ingredient.request';

export class CreateProductRequest  {

 	productName: string;
	price: number;
    productIngredients: ProductIngredientRequest[];

}