import { HttpParams } from "@angular/common/http";
import { SortDirectionEnum } from "../components/datatable/sort-direction.enum";

export class PaginatedRequest {
    pageNum: number;
    pageSize: number;
	sortBy: string;
	search: string;
	sortDirection: SortDirectionEnum

	public ToHttpParams(): HttpParams {

        let httpParams = new HttpParams();
        httpParams = httpParams.set('pageNum', this.pageNum.toString());
        httpParams = httpParams.set('pageSize', this.pageSize.toString());

		if (this.search)
			httpParams = httpParams.set('search', this.search.toString().trim());


        if (this.sortDirection)
            httpParams = httpParams.set('sortDirection', this.sortDirection.toString());

        if (this.sortBy)
            httpParams = httpParams.set('sortBy', this.sortBy);

        return httpParams;
    }
}