import { UnitOfMeasureEnum } from '../enums/unit-of-measure.enum';
import { ProductIngredientRequest } from './product-ingredient.request';

export class UpdateProductRequest  {

	productId: number;
 	productName: string;
	price: number;
    productIngredients: ProductIngredientRequest[];

}