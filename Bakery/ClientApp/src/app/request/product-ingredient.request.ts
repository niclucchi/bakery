import { UnitOfMeasureEnum } from '../enums/unit-of-measure.enum';

export class ProductIngredientRequest  {

	ingredientId:number;
	quantity:number;
	unitOfMeasure: UnitOfMeasureEnum;

}