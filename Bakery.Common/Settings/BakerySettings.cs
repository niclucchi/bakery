﻿namespace Bakery.Common.Settings
{
    public class BakerySettings
    {
        public string JwtKey { get; set; }
        public string JwtIssuer { get; set; }
    }
}
