﻿using System.Collections.Generic;

namespace Bakery.DTO.Request
{
    public class CreateProductRequest
    {
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public List<ProductIngredientRequest> ProductIngredients { get; set; }
    }
}
