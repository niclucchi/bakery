﻿
namespace Bakery.DTO.Request
{
    public class ProductsOnSalePaginatedRequest : PaginatedRequest
    {
        public bool HideExpiredProducts { get; set; }
    }
}
