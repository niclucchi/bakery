﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bakery.DTO.Request
{
    public class CreateProductOnSaleRequest
    {
        public int ProductId { get; set; }
        public int Quantity { get; set; }
    }
}
