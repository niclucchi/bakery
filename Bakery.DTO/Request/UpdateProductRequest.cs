﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bakery.DTO.Request
{
    public class UpdateProductRequest
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public List<ProductIngredientRequest> ProductIngredients { get; set; }
    }
}
