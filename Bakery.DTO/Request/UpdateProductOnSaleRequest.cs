﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bakery.DTO.Request
{
    public class UpdateProductOnSaleRequest
    {
        public int ProductOnSaleId { get; set; }
        public int Quantity { get; set; }
    }
}
