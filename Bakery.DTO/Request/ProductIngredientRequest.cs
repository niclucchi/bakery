﻿using Bakery.Common.Enums;

namespace Bakery.DTO.Request
{
    public class ProductIngredientRequest
    {
        public int IngredientId { get; set; }
        public double Quantity { get; set; }
        public UnitOfMeasureEnum UnitOfMeasure { get; set; }
    }
}
