﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Bakery.DTO.Request
{
    public class PaginatedRequest
    {
        public int PageNum { get; set; }
        public int PageSize { get; set; }
        public ListSortDirection SortDirection { get; set; }
        public string SortBy { get; set; }
        public string Search { get; set; }


        public string GetSorting()
        {
            if (!string.IsNullOrWhiteSpace(SortBy))
            {
                string sortDirectionStr = SortDirection == ListSortDirection.Ascending ? "ASC" : "DESC";
                return $"{SortBy} {sortDirectionStr}";
            }

            return string.Empty;
        }
    }
}
