﻿namespace Bakery.DTO.Response
{
    public class AuthenticationResponse
    {
        public string Token { get; set; }
    }
}
