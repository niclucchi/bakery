﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bakery.DTO.Response
{
    public class ProductOnSaleResponse
    {
        public int ProductOnSaleId { get; set; }
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public DateTime PutUpForSaleDateUtc { get; set; }
        public decimal FullPrice { get; set; }
        public decimal? DiscountedPrice { get; set; }
        public int Quantity { get; set; }
        public decimal TotalFullPrice => FullPrice * Quantity;
        public decimal? TotalDiscountedPrice => DiscountedPrice * Quantity;
        public int DaysFromPutOnSaleDate => Convert.ToInt32((DateTime.UtcNow.Date - PutUpForSaleDateUtc).TotalDays);
        public bool IsExpired { get; set; }
    }
}

