﻿using System.Collections.Generic;

namespace Bakery.DTO.Response
{
    public class ProductDetailResponse : ProductResponse
    {
        public List<ProductIngredientResponse> ProductIngredients { get; set; }
    }
}
