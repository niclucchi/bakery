﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bakery.DTO.Response
{
    public class IngredientReponse
    {
        public int IngredientId { get; set; }
        public string Name { get; set; }
    }
}
