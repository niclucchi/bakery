﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bakery.DTO.Response
{
    public class PaginatedResponse<T>
    {
        public List<T> Data { get; set; }
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public long NumberOfPages { get; set; }
        public long TotalRecords { get; set; }
    }
}
