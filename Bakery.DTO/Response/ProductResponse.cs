﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bakery.DTO.Response
{
    public class ProductResponse
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal Price { get; set; }
    }
}
