﻿using Bakery.Common.Enums;

namespace Bakery.DTO.Response
{
    public class ProductIngredientResponse
    {
        public int ProductIngredientId { get; set; }
        public double Quantity { get; set; }
        public UnitOfMeasureEnum UnitOfMeasure { get; set; }
        public IngredientReponse Ingredient { get; set; }
    }
}
